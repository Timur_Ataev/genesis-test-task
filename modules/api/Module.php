<?php

namespace app\modules\api;

use yii\base\BootstrapInterface;
use yii\base\InvalidConfigException;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'app\modules\api\controllers';

   public function bootstrap($app)
   {
       if ($app instanceof \yii\web\Application) {
           $app->getUrlManager()->addRules([
               ['class' => 'yii\rest\UrlRule', 'controller' => 'api/client'],
               ['class' => 'yii\rest\UrlRule', 'controller' => 'api/address'],
           ]);
       } else {
           throw new InvalidConfigException('Can use for web application only.');
       }
   }

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
