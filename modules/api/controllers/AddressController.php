<?php

namespace app\modules\api\controllers;

use app\modules\api\models\Address;
use yii\rest\ActiveController;

class AddressController extends ActiveController
{
    public $modelClass = Address::class;
}