<?php

namespace app\modules\api\controllers;

use app\modules\api\models\Client;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class ClientController extends ActiveController
{
    public $modelClass = Client::class;

    public function actions()
    {
        $actions = parent::actions();

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Client::find()->with('address'),
        ]);
    }
}