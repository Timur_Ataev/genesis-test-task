<?php

namespace app\modules\api\models;

use yii\behaviors\TimestampBehavior;
use yii\redis\ActiveRecord;

class Address extends ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function attributes()
    {
        return ['id','city','address','created_at','updated_at'];
    }

    public function rules()
    {
        return [
            [['city','address'],'safe']
        ];
    }
}