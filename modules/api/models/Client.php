<?php

namespace app\modules\api\models;

use yii\behaviors\TimestampBehavior;
use yii\redis\ActiveQuery;
use yii\redis\ActiveRecord;

class Client extends ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function attributes()
    {
        return ['id','name','phone','age','address_id','created_at','updated_at'];
    }

    public function rules()
    {
        return [
            [['name','phone','age','address_id'],'safe']
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['address'] = function($model) {
            return $model->address;
        };
        return $fields;
    }

    /**
    * @return ActiveQuery defines a relation to the Address
    */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id'=>'address_id']);
    }
}